<?php

require_once ("sheep.php");
require_once ("frog.php");
require_once ("age.php");

$sheep = new sheep("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Leg : " . $sheep->leg . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>";

$buduk = new frog("buduk");
echo "Name : " . $buduk->name . "<br>";
echo "Leg : " . $buduk->leg . "<br>";
echo "cold blooded : " . $buduk->cold_blooded . "<br>";
$buduk->jump("Hop Hop");
echo "<br><br>";

$kera_sakti = new age("Kera sakti");
echo "Name : " . $kera_sakti->name . "<br>";
echo "Leg : " . $kera_sakti->leg . "<br>";
echo "cold blooded : " . $kera_sakti->cold_blooded . "<br>";
$kera_sakti->yell("Auoo");

?>